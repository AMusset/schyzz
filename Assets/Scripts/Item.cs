﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class ItemEnvironmentPos{
//	public int x;
//	public int y;
//	public float z;
//	public int mur;
//	
//	public void setPos (int xPos, int yPos, int nmur) {
//		
//	}
//}

public class Item : MonoBehaviour, IInventoryItem {
	
	//public void toInventory(Item ei) {
	//}
	//
	//public void toEnvironment(Item ii) {
	//	
	//}
	//
	////Constructeur
	//static void Main(string[] args) {
    //  Item item = new Item();    
    //  
	//}
	
	public string Name
	{
		get
		{
			return "ItemName";
		}
	}
	
	public Sprite _Image;
	
	public Sprite Image
	{
		get 
		{ 
			return _Image; 
		}
	}
	
	public void OnPickup()
	{
		gameObject.SetActive(false);
	}
	
	public void OnDrop()
	{
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
		RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
		Debug.Log("Droping");
		// RaycastHit hit = new RaycastHit();
		// Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		gameObject.SetActive(true);
		gameObject.transform.position = mousePos2D;
		
		// if(Physics.Raycast(ray, out hit, 1000))
		// {
		// 	
		// }
	}
}