﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHandler : MonoBehaviour {
	
	// Canvas Scaler : UI Scale Mode : Scale With screen Size
	
	public void Activate () {
		Debug.Log("I am activated!");
	}
	
	public void ToTheRight () {
		GameObject GameInterface = GameObject.Find("GameITF");
		if (GameInterface.transform.position.x > 150) {
			Vector3 temp = new Vector3(-150f,0,0);
			GameInterface.transform.position += temp;
		} else {
			Vector3 temp = new Vector3(50f,0,0);
			GameInterface.transform.position += temp;
		}
		Debug.Log("Right working");
	}
	
	public void ToTheLeft () {
		GameObject GameInterface = GameObject.Find("GameITF");
		if (GameInterface.transform.position.x < 25) {
			Vector3 temp = new Vector3(150f,0,0);
			GameInterface.transform.position += temp;
		} else {
			Vector3 temp = new Vector3(-50f,0,0);
			GameInterface.transform.position += temp;
		}
		Debug.Log("Left working");
	}
	
}
