﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryHandler : MonoBehaviour {
	
	public void InventoryUp () {
		GameObject GameInventory = GameObject.Find("Inventory");
		if (GameInventory.transform.position.y > -220) {
			Vector3 temp = new Vector3(0,-71,0);
			GameInventory.transform.position += temp;
		}
		Debug.Log("Up working");
	}
	
	public void InventoryDown () {
		GameObject GameInventory = GameObject.Find("Inventory");
		if (GameInventory.transform.position.y < 750) {
			Vector3 temp = new Vector3(0,+71,0);
			GameInventory.transform.position += temp;
		}
		Debug.Log("Down working");
	}
	
}
