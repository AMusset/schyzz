﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHandler : MonoBehaviour {
	
	public Inventory inventory;
	
	// Elements à ne pas oublier pour que ça fonctionne : 
	// Script / Button / Image (Raycast Traget) sur chaque object interactible
	// Physics 2D Raycaster
	// Cocher "Interactable"
	
	public void onMouseDown() {
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
        RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
		
        if (hit.collider != null) {
			//Debug.Log(hit.collider.gameObject.name);
			//GameObject inventoryTiles = GameObject.Find("InventoryTiles");
			//Transform goTransform;
			//goTransform = inventoryTiles.transform;
			//
			//hit.collider.gameObject.transform.SetParent(goTransform, false);
			//hit.collider.gameObject.transform.SetPositionAndRotation(new Vector3(16,0,0), Quaternion.Euler(new Vector3(0,0,0)));
			//hit.collider.gameObject.transform.localScale += new Vector3(0.5f,0.5f,0.5f);
			IInventoryItem item = hit.collider.GetComponent<IInventoryItem>();
			if (item != null)
			{
				inventory.AddItem(item);
			}
			
        }
	}
	
}
