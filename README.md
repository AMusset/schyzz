# Schyzz

Partage du Projet Campus Factory : "Schyzz"
Autorisés à la lecture / écriture : 
- MUSSET Alexis
- GOUHIER Titouan
- HOUSSEAU Jérémy
- HOLVECK Tom
- CROS Lewis
- VAIGREVILLE Pierre
- LEMOINE Lilian
- PALARD Déborah

OBTENIR LE PROJET SUR VOTRE APPAREIL : Télechargez le simplement en .zip

OBTENIR LE PROJET VERSION GIT PROJECT.
- Créez vous un dossier spécialement dédié aux repository git
- Dans ce dossier ouvert : Clic droit > Git bash here (obtenir git peut être nécessaire)
- $ git clone <lien copié avec le bouton clone sur la page web du projet>
